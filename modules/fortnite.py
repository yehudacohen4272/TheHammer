"""
    The Hammer
    Copyright (C) 2018 JustMaffie

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published
    by the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program. If not, see <http://www.gnu.org/licenses/>.
"""

from discord.ext import commands
import discord
import datetime
from thehammer.module import Module
import random

now = lambda: datetime.datetime.utcnow()


class Fortnite(Module):
    def get_value(self, list, key):
        for entry in list:
            if entry['key'] == key:
                return entry
        return {}

    async def generate_embed(self, author, data):
        color = discord.Colour.blue()
        embed = discord.Embed(colour=color, timestamp=now())
        name = self.bot.user.name
        avatar = self.bot.user.avatar_url
        embed.set_author(name=name, icon_url=avatar)
        embed.add_field(name="Platform", value=data['platformNameLong'])
        embed.add_field(name="Username", value=data['epicUserHandle'])
        stats = data['lifeTimeStats']
        wins = self.get_value(stats, "Wins")['value']
        played = self.get_value(stats, "Matches Played")['value']
        winPercent = self.get_value(stats, "Win%")['value']
        kills = self.get_value(stats, "Kills")['value']
        kdr = self.get_value(stats, "K/d")['value']
        embed.add_field(name="Wins", value=str(wins))
        embed.add_field(name="Matches Played", value=str(played))
        embed.add_field(name="Win Percentage", value=str(winPercent))
        embed.add_field(name="Kills", value=str(kills))
        embed.add_field(name="Kill/Death Ratio", value=str(kdr))
        avatar = author.avatar_url
        msg = 'Requested by: {}'
        embed.set_footer(text=msg.format(author), icon_url=avatar)
        return embed

    @commands.group()
    async def fortnite(self, ctx):
        if ctx.invoked_subcommand is None:
            await ctx.send_help()

    @fortnite.command()
    async def shop(self, ctx, embed=None):
        if embed == "embed":
            shop = await self.bot.fortnite.get_shop_embed()
        else:
            shop = await self.bot.fortnite.get_shop_image()
        await shop.send(ctx)

    @fortnite.command()
    async def cosmetic(self, ctx, *, name):
        cosmetic = await self.bot.fortnite.get_cosmetic(name)
        await cosmetic.send(ctx)

    @fortnite.command()
    async def stats(self, ctx, platform: str, *, username):
        if not hasattr(self.bot, "owner"):
            message = "Hey, I'm sorry, but I am not ready yet, "
            "please try again in a few seconds."
            return await ctx.send(message)
        stats = await self.bot.fortnite.get_stats(username, platform)
        if isinstance(stats, str):
            return await ctx.send("{error}".format(error=stats))
        embed = await self.generate_embed(ctx.author, stats)
        return await ctx.send(embed=embed)

    @fortnite.command()
    async def random(self, ctx, type=None):
        types = ['backpack', 'glider', 'pickaxe', 'skin', 'loading', 'outfit']
        if not type:
            type = random.choice(types)
        type = type.lower()
        nice_types = [type.capitalize() for type in types]
        if not type in types:
            message = "Invalid type, valid types are: `{types}`"
            types = "`, `".join(nice_types)
            return await ctx.send(message.format(types=types))
        if type == "skin":
            type = "outfit"
        cosmetic = await self.bot.fortnite.random_cosmetic(type)
        await cosmetic.send(ctx)

    @fortnite.command()
    async def news(self, ctx, *, gamemode=None):
        nice_gamemodes = ["BR", "Battle Royale", "STW", "Save The World"]
        nice_gamemodes = "`, `".join(nice_gamemodes)
        if not gamemode:
            message = "Invalid gamemode, valid gamemodes are: `{gamemodes}`"
            return await ctx.send(message.format(gamemodes=nice_gamemodes))
        gamemodes = ['br', 'battle royale', 'stw', 'save the world']
        gamemode = gamemode.lower()
        if not gamemode in gamemodes:
            message = "Invalid gamemode, valid gamemodes are: `{gamemodes}`"
            return await ctx.send(message.format(gamemodes=nice_gamemodes))
        if gamemode == "br" or gamemode == "battle royale":
            gamemode = "battleroyale"
        else:
            gamemode = "savetheworld"
        news = await self.bot.fortnite.get_news(gamemode)
        await news.send(ctx)


def setup(bot):
    bot.load_module(Fortnite)
